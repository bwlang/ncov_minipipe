################################################################################
# GENERAL CONFIGURATION                                                        #
################################################################################

# Please provide the absolute path of the directory to be used to store all data.

output: /path/to/project/

# Please provide the absolute path of the YAML-formatted file that contains all
# sample information.

samples: /path/to/project/sample.config.yaml


################################################################################
# REFERENCE GENOME                                                             #
################################################################################

# Please provide the absolute path of the FASTA file containing the reference 
# genome sequence. If this value is empty, NC_045512.2 (Severe acute respiratory
# syndrome coronavirus 2 isolate Wuhan-Hu-1, complete genome) is used as a 
# reference.

reference: /path/to/reference.fasta


################################################################################
# TAXONOMIC READ FILTER                                                        #
################################################################################

# Please provide the absolute path to the kraken2 database folder to exclude
# all reads not classified as SARS-COV-2 (NCBI taxonomy ID 2697049) from read 
# mapping. A pre-processed kraken2 database can be downloaded from: 
#
# https://zenodo.org/record/3854856 
#
# (please unpack before usage and use taxID 2697049 together with this database).
# Leave these values empty to skip taxonomic read filtering.

krakenDb: /path/to/krakenDb_folder
krakenTaxID: 2697049 


################################################################################
# AMPLICON PRIMER CLIPPING                                                     #
################################################################################

# Please provide the absolute path to the amplicon bedpe file that defines the
# genomic positions of used amplicon primer. The BEDPE format is defined here: 
# https://bedtools.readthedocs.io/en/latest/content/general-usage.html#bedpe-format
# Leave this value empty to skip primer clipping.

primer: /path/to/primer.bedpe


################################################################################
# ADAPTER CLIPPING                                                             #
################################################################################

# Please define the absolute path of a FASTA file containing the adapter
# sequences to be clipped. Leave this value empty to skip adapter clipping.

adapter: /path/to/adapter.fasta


################################################################################
# VARIANT CALLING                                                              #
################################################################################

# Please define the minimum number of reads at a position to be considered for 
# variant calling.

var_call_cov: 20

# Please define the minimum number of supporting reads which are required to 
# call a variant.

var_call_count: 10

# Please define the minimum percentage of supporting reads at the respective 
# position required to call a variant. In turn, variants supported by 
# (1 - var_call_frac)*100% reads will be explicitely called.

var_call_frac: 0.1


################################################################################
# VARIANT HARD FILTERING                                                       #
################################################################################

# Please define the minimal mean mapping quality of observed alternate alleles
# (MQM). The mapping quality (MQ) measures how good reads align to the 
# respective reference genome region. Good mapping qualities are around MQ 60. 
# GATK recommends hard filtering of variants with MQ less than 40. 

var_filter_mqm: 40


# Please define the strand balance probability for the alternate allele (SAP). 
# The SAP is the Phred-scaled probability that there is strand bias at the 
# respective site. A value near 0 indicates little or no strand bias. 

var_filter_sap: 60

# Please define the minimal variant call quality. 
# Freebayes produces a general judgement of the variant call.

var_filter_qual: 10


################################################################################
# CONSENSUS GENERATION                                                         #
################################################################################

# Please define the minimum number of reads required so that the respective 
# position in the consensus sequence is NOT hard masked.

cns_min_cov: 20

# Please define a  minimum fraction of reads supporting a variant which leads to
# an explicit call of this variant (genotype adjustment). The value has to be
# greater than 0.5 but not greater than 1. To turn genotype adjustment this off,
# leave the value empty.

cns_gt_adjust: 0.9


################################################################################
# PHYLOGENETIC LINEAGE ASSIGNMENT                                              #
################################################################################

# Pangolin can be used to assign SARS-CoV-2 lineages to reconstructed sequences
# (for installation of Pangolin see here: 
# https://github.com/cov-lineages/pangolin#install-pangolin). Please provide
# the name of the Pangolin conda environment. Leave this empty value, to skip
# lineage assignment.

pangolin: pangolin


################################################################################
# REPORT                                                                       #
################################################################################

# Please define the absolute path of the GFF3 (GTF, GFF) file containing the reference 
# genome annotation. This information is used to infer the effect of detected
# SNPs and InDels on coding sequences. In addition, the annotation will be used to 
# perform a lift-over of the annotated genes to the calculated consensuses. 
# Leave this value empty to skip variant inspection and gene annotation.

cns_annotation: /path/to/annotation.gff

# Toggle for annnotation  
var_annotation: True


# Variation file used to flag special interest variation sites.
# Important for quality control and to report back variants in an expedient
# manner
var_special_interest: /path/to/var_of_interest.vcf
  
# Please define a comma-seperated list of run ids to include in the report.  
# Leave this value empty to not include any run id.

run_id: 

